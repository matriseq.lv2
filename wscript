#!/usr/bin/env python
import waflib.extras.autowaf as autowaf
import re

MATRISEQ_VERSION = '1.0.0'

# Mandatory waf variables
APPNAME = 'matriseq'        # Package name for waf dist
VERSION = MATRISEQ_VERSION  # Package version for waf dist
top     = '.'               # Source directory
out     = 'build'           # Build directory

def options(opt):
    opt.load('compiler_c')
    opt.load('lv2')
    autowaf.set_options(opt)

def configure(conf):
    autowaf.display_header('Matriseq Configuration')
    conf.load('compiler_c', cache=True)
    conf.load('lv2', cache=True)
    conf.load('autowaf', cache=True)
    autowaf.set_c_lang(conf, 'c99')

    autowaf.check_pkg(conf, 'lv2', atleast_version='1.0.0', uselib_store='LV2')

    # Set env.pluginlib_PATTERN
    pat = conf.env.cshlib_PATTERN
    if pat.startswith('lib'):
        pat = pat[3:]
    conf.env.pluginlib_PATTERN = pat
    conf.env.pluginlib_EXT = pat[pat.rfind('.'):]

    autowaf.display_summary(conf)
    autowaf.display_msg(conf, "LV2 bundle directory", conf.env.LV2DIR)
    print('')

def build(bld):
    bundle = 'matriseq.lv2'

    # Build manifest.ttl by substitution (for portable lib extension)
    bld(features     = 'subst',
        source       = 'manifest.ttl.in',
        target       = '%s/%s' % (bundle, 'manifest.ttl'),
        install_path = '${LV2DIR}/%s' % bundle,
        LIB_EXT      = bld.env.pluginlib_EXT)

    # Copy other data files to build bundle (build/matriseq.lv2)
    for i in ['matriseq.ttl']:
        bld(features     = 'subst',
            source       = i,
            target       = '%s/%s' % (bundle, i),
            install_path = '${LV2DIR}/%s' % bundle,
            LIB_EXT      = bld.env.pluginlib_EXT)

    # Build plugin library
    obj = bld(features     = 'c cshlib',
              source       = ['matriseq.c'],
              name         = 'matriseq',
              target       = '%s/matriseq' % bundle,
              install_path = '${LV2DIR}/%s' % bundle,
              includes     = ['.'],
              lib          = ['pthread'])
    autowaf.use_lib(bld, obj, 'LV2')
    obj.env.cshlib_PATTERN = re.sub('^lib', '', bld.env.cshlib_PATTERN)

